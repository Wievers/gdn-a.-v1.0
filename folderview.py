import os
import os.path as pth
import subprocess as sp

class folderView:
    """This class emulates a file system, with the files and folders, from a root to a certain depth"""
    possExt = ["Music", "Text", "Code", "Video", "Images", "Archive", "Misc", "System"]
    def __init__(self, iterFw = None, listComplete = [], list_verif = []):
        """ This method creates a new folderview instance.
            - gen : A generator to create the folderview or a list that
                represents the folderview (Ex: [file1, (nameFold1, [file11, file12]), ...])
            - listComplete : A list of the filenames of the folderview (Ex : [file1, folder1/file11, folder1/file12, ...])
            - list_verif   : A list to verify the folder names of the folderView (HIGHLY recommended while using the
                                folderview, specially if you have empty folders.)"""
        self.partialDictFiles = None
        if iterFw is None and len(listComplete) == 0:
            self.partialDictFiles = []

        elif iterFw is None and len(listComplete) != 0:
            self.partialDictFiles = self.walk(listComplete, list_verif)

        elif iterFw is not None:
            self.partialDictFiles = self.convert(iterFw)

        else:
            raise fvException("You can't create a folderview without a list or a generator")
        self.stackOp = None

    def __iter__(self):
        """Base method that permit the user to iterate a folderView (Example : using for x in MyFolderView: print(x) ) """
        if self.partialDictFiles is None:
            raise fvException("Bad Generator Conversion")
        return iter(self.partialDictFiles)

    @classmethod
    def extension(cls):
        return cls.possExt

    def convert(self, generator):
        """This method is converting a folderView in a list of dictionnary using the same norm as the read method.
            - return : A list of the dictionnaries representing the files and folders in the directory represented by the folderView"""

        def s_convert(dictionnaryList = [], gene = None, relaPath = ""):
            """Sub-function converting the folderView in a list of dictionnary, as specified in the main function
                - dictionnaryList : Our list of dictionnaries returned at the end. See convert Doc for more infos
                - gene : Parameter for recursive purpose only (should stay default). It's the generator that represents folder
                - relaPath : Parameter for recursive purpose (should stay default). The relative path that will be given to each folder/file (root is the 1st generator"""
            for x in gene:
                if type(x) != str:
                    dicList = []
                    dictionnaryList.append({"Name" : x[0], "Path" : relaPath, "isFile" : False, "Files" : dicList})
                    s_convert(dicList, x[1], pth.join(relaPath,x[0])) #We read the generator
                else:
                    if x.rfind(".") != -1:
                        ext = x[x.rfind(".")+1:]
                    else:
                        ext = "txt"
                    dictionnaryList.append({"Name" : x, "Path" : relaPath, "isFile" : True, "Extension" : ext })
        listGen = []
        s_convert(listGen, generator)
        return listGen

    def walk(self, raw_data, v_folders = []):
        """ This method transform a list with raw filenames with directories into datas that can 'nearly' be analyze by a folderview
            instance.
                raw_data : list of all the files and folders (and empty folders) to analyze
                v_folders [OPTIONAL]: list of the folders of the server (to verify and avoid empty folders recognized as files)
        NOTE : This method is for internal use only. Don't use it"""
            #On définit la fonction qui avancera d'un dossier à un dossier
        def walkGraph(tree, v_tree):
            pivots = [] #On va chercher les occurences des noms de cette liste
            t_buffer = [] #Juste une liste tampon :)
            t_format = [] #Le tree (graphe), qu'on aura 'formaté' une fois
            for x in tree:
                if x.find("/") == -1:
                    pivots.append(x)
                else:
                    t_buffer.append(x)

            tree = t_buffer

            for x in pivots: #On va chercher les pivots qui reviennent dans les autres filenames
                t_buffer = []
                for y in tree:
                    if x == y[:y.find("/")]: #Nous cherchons les occurences dans la première partie du filename, c'est à dire avant le "/"
                            #Nous ne voulons pas remettre cette partie que l'on a vérifié sinon, on se retrouve avec un problème au
                            #prochain tour. On la supprime donc avant de la rajouter à la liste des occurences
                        t_buffer.append(y[y.find("/")+1:])
                if len(t_buffer) != 0 or x in v_tree: #Si le pivot a une occurence, on peut dire que c'est un dossier, et donc en faire une liste
                        #avec son nom, et la liste des occurences trouvée à l'intérieur
                    t_format.append([x, t_buffer])
                else: #Si le pivot n'a aucune occurence, alors c'est un fichier (si on connait depth+1)
                    t_format.append(x)
            return t_format #On retourne la liste avec les "feuilles" (fichiers) et les "branches" (dossier)
            #On définit la fonction qui formatera ma liste finale
        def formate(liste):
            """This method format the list you input to a folderview-useable list. The usage is internal, as the liste given HAVE to be a list
            process by self.walk()."""
            l_buffer = []
            b_depth = liste[-1] #L'algorithme de parcours implémenté dans le SRP nous met les dossiers les plus 'profonds' en fin
                    #de liste. D'ou le -1.
            ver = False
            for x in liste:
                if b_depth[0] in x[1]:
                        #On supprime les parties que l'on a trouvé pour éviter les doublons
                    x[1].remove(b_depth[0])
                    for t in b_depth[1]:
                        if type(t) is str:
                            x[1].remove(pth.join(b_depth[0], t))
                            #Ajout du dossier en tant que sous-dossier
                        else:
                            x[1].remove(pth.join(b_depth[0], t[0]))
                    x[1].append(b_depth)
                    ver = True #Nous avons réussi à faire rentrer un dossier
            if ver == False: #Nous n'avons pas réussi à faire rentrer un dossier dans un autre, donc on arrête.
                return (not ver)
            liste.remove(b_depth) #On supprime le sous-dossier de la liste générale, pour éviter les doublons
            formate(liste)

        queue = [] #On crée une file FIFO (First In, First Out) pour gérer les sommets au fur et à mesure
        fin = []
        root = True
        queue.append(raw_data)
        while len(queue) != 0: #Tant que la file n'est pas vide, il y'a des sommets à traiter
            raw_data = queue.pop(0) #On sort donc le premier sommet
            buffer = walkGraph(raw_data, v_folders) #On cherche toutes les branches/feuilles qu'il peut avoir (donc de potentiels autres sommets)

            for x in buffer:
                if type(x) is list:
                    x = tuple(x)  #Le tuple est la norme du SRP et des folderviews, ne l'oublions pas.
                    queue.append(x[1]) #On append à la file la LISTE et non le tuple, qui est à usage externe.
                    fin.append(x)
                else:
                    if root:
                        fin.append(x)
            root = False #On veut récupérer les fichiers du root en tant "qu'indépendant" dans notre liste, et non les autres.
        formate(fin)
        return self.convert(fin)

    def sort(self, keyWord, dictList = None):
        """Sort the first layer of a folderView by using quicksort.
            - keyWord : It's the term by which all the folderview will be sorted (the key of the dictionnary)
                      example: "Path" is a key, and we will sort every dictionnary by the length of the path
            - dictList : Parameter for recursive fundtion. Should remain default"""
        if dictList is None:
            if self.partialDictFiles is None:
                raise fvException("Bad generator conversion")
            else:
                if self.stackOp is None:
                    self.stackOp = self.partialDictFiles
                dictList = self.stackOp

        less = []
        equal = []
        greater = []

        for x in dictList:
            if keyWord not in x.keys() or keyWord == "Files":
                raise fvException("Invalid Keyword")

        if len(dictList) > 1:
            pivot = dictList[0]
            try:
                for x in dictList:
                    if len(x[keyWord]) < len(pivot[keyWord]):
                        less.append(x)
                    elif len(x[keyWord]) == len(pivot[keyWord]):
                        equal.append(x)
                    elif len(x[keyWord]) > len(pivot[keyWord]):
                        greater.append(x)

            except KeyError as e:
                print("Bad Key to the dictionnary, typo?", e)
            except TypeError as e:
                print("Bad type in the dictionnary:", e)

            self.stackOp = self.sort(keyWord, less)+equal+self.sort(keyWord, greater)
            return self.stackOp  # Just use the + operator to join lists
        else:  # You need to handle the part at the end of the recursion - when you only have one element in your array, just return the array.
            return dictList

    def reload(self, generator):
        """This method reload the folderView by deleting the attributes (aside the generator)
            - generator : The new generator that will (re)initialize the folderView"""
        self.partialDictFiles = self.convert(generator)
        self.stackOp = None

    def last(self):
        """This method return the last result we have in the stack"""
        if self.stackOp is None:
            return self.partialDictFiles
        else:
            return self.stackOp

    def search(self, itemWord, keyWord = None):
            """ This method search in a folderview the file/folder with the keyWord you pass in, and gives you all the details of the said file/folder
                - itemWord : A caracteristic of the searched file (like the name)
                - keyWord : To perform a faster research by giving the key of the item you want to match
                - return : The file/folder or the first match if the keyWord matches multiples files/folders"""
            if self.partialDictFiles is None:
                raise fvException("Bad generator conversion")
            queue = [] #FIFO queue
            s_results = []
            for elem in self.partialDictFiles:
                queue.append(elem)
                while len(queue) != 0:
                    buffer = queue.pop()
                        #Si notre buffer est un dossier on met ses 'enfants' dans la queue
                    if not buffer["isFile"]:
                        queue = queue + buffer["Files"]

                    if keyWord is not None:
                        if keyWord in ["Extension"] and buffer["isFile"] and buffer[keyWord] == itemWord:
                            s_results.append(buffer)
                        elif keyWord not in ["Extension"] and buffer[keyWord] == itemWord:
                            s_results.append(buffer)

                    else:
                        for key in buffer.keys():              #Ca va TOUT trouver, c'est à dire que si on cherche un dossier
                            if str(buffer[key]).strip() == itemWord: #On trouvera aussi tous ses 'enfants' directs (à cause du path)
                                s_results.append(buffer)

            return s_results

    def findFiles(self, listName = None, keyWord = None):
        """ This method find all the files available in a folderView, or the list of files that we specify
             - listName : List of the matches we would like in the folderView
             - keyWord : To sort the files with a critera
                          WARNING : allFiles works only if there is no partialDictFiles define. You may want to reload the folderview before using it"""
        nameFound = []
        nf_buffer = self.search(True, "isFile")
        for dic in nf_buffer:
            if listName is not None and dic["Name"] in listName:
                nameFound.append({key:value for key, value in dic.items() if key in {"Name", "Path", "Extension"}})
            elif listName is None:
                nameFound.append({key:value for key, value in dic.items() if key in {"Name", "Path", "Extension"}})

        self.stackOp = nameFound #Important! We are updating our stack. Some methods needs it absolutely!
        if keyWord is None:
            return self.stackOp
        else:
            return self.sort(keyWord)

    def findFold(self, listName = None, keyWord = None):
        """ This method find all the folders available in a folderView, or the list of folders we specify
             - listName : List of the matches we would like (and hopefully will have) in the folderView
             - allFolders : If we search only the folders in this folderview, or all the folders in the filesystem
                          WARNING : allFiles works only if there is no partialDictFiles define. You may want to reload the folderview before using it"""
        nameFound = []
        nf_buffer = self.search(False, "isFile")
        for dic in nf_buffer:
            nameFound.append({key:value for key, value in dic.items() if key in {"Name", "Path"}})

        self.stackOp = nameFound #Important!
        if keyWord is None:
            return self.stackOp
        else:
            return self.sort(keyWord)

    def extSearch(self, key_Ext = None, list_Ext = None):
        nameFound = []
        nf_buffer = []

        ext_pattern = {i:[] for i in self.possExt}
        ext_pattern[self.possExt[0]] = ["aif", "aiff", "pcm", "aac", "mp3", "wav", "flac", "alac", "cda", "iff", "mid", "midi", "mpa", "wma", "wpl"]
        ext_pattern[self.possExt[1]] = ["txt", "odt", "doc", "docx", "pdf", "rtf", "asc", "csv"]
        ext_pattern[self.possExt[2]] = ["py", "cpp", "c", "h", "hpp", "htm", "html", "css", "php", "xml", "pl", "cfg", "log", "tex"]
        ext_pattern[self.possExt[3]] = ["mp4", "avi", "m4v", "mkv", "mpg", "mpeg", "webm", "ogg", "mov"]
        ext_pattern[self.possExt[4]] = ["jpeg", "jpg", "png", "gif", "ico", "bmp", "tiff", "webp", "ppm", "pgm", "pbm", "pnm"]
        ext_pattern[self.possExt[5]] = ["tar", "gz", "bz", "gz2", "bz2", "rar", "7z", "zip", "ipg", "z"]
        ext_pattern[self.possExt[6]] = ["ppt", "pps", "block", "blend" "c4d", "ssh", "pub", "ppk"]
        ext_pattern[self.possExt[7]] = ["exe", "sh", "raw", "bak", "dat", "dsk", "bin"]
        if key_Ext is None and list_Ext is None:
            raise fvException("extSearch() needs at least one of the to parameters")

        elif (type(key_Ext) is not str and key_Ext is not None) or (type(list_Ext) is not list and list_Ext is not None):
            raise TypeError("Incorrect type given: " + str(type(list_Ext)) + " " + str(type(key_Ext)) + " instead of str and list")

        elif key_Ext not in ext_pattern.keys():
            raise KeyError("This key " + key_Ext + " doesn't exists!")

        elif key_Ext is not None and type(key_Ext) is str:
            for x in ext_pattern[key_Ext]:
                nf_buffer = nf_buffer + self.search(x, "Extension")
            for dic in nf_buffer:
                nameFound.append({key:value for key, value in dic.items() if key in {"Name", "Path"}})

        elif key_Ext is None and list_Ext is not None and type(list_Ext) is list:
            for x in list_Ext:
                try:
                    nf_buffer = nf_buffer + self.search(x, "Extension")
                except Exception as e:
                    raise fvException(e)
            for dic in nf_buffer:
                nameFound.append({key:value for key, value in dic.items() if key in {"Name", "Path"}})

        return nameFound

    def patternSearch(self, pattern, keyWord = None):
        if self.partialDictFiles is None:
            raise fvException("Bad generator conversion")
        queue = [] #FIFO queue
        s_results = []
        for elem in self.partialDictFiles:
            queue.append(elem)
            while len(queue) != 0:
                buffer = queue.pop()
                    #Si notre buffer est un dossier on met ses 'enfants' dans la queue
                if not buffer["isFile"]:
                    queue = queue + buffer["Files"]

                if keyWord is not None:
                    if keyWord in ["Extension"] and buffer["isFile"] and pattern in buffer[keyWord]:
                        s_results.append(buffer)
                    elif keyWord not in ["Extension"] and pattern in buffer[keyWord]:
                        s_results.append(buffer)

                else:
                    for key in buffer.keys():            #Ca va TOUT trouver, c'est à dire que si on cherche un dossier
                        if pattern in str(buffer[key]).strip(): #On trouvera aussi tous ses 'enfants' directs (à cause du path)
                            s_results.append(buffer)

        return s_results

def localFilesList(filename = "/home/wieves/Programs/Python/SRP", depth = 0):
        """Create a generator of the filesystem (root = SRP default directory) on the local computer """
        if filename is None:
            filename = self.settings[1]
        try:
            os.chdir(filename)
        except FileNotFoundError as e:
            raise exceptionSSH("Can't get to the directory: " + e)
        #files = [x for x in os.listdir()]
        for x in os.listdir():
            #print(pth.join(filename, x))
            #print(pth.isfile(pth.join(filename,x)))
            #print(filename)
            if pth.isfile(pth.join(filename, x)):
                yield x
            else:
                yield x, localFilesList(pth.join(filename,x), depth + 1)


class fvException(Exception):
        pass
