import sys
from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QAbstractItemModel
from PySide2 import QtGui as qtg

import prgmNotes as nt

class fenetreAjout(qtw.QDialog):
    """Cette classe gèrant les inputs"""
    def __init__(self, control, add_sub = False, parent=None):
        #type = "sub" ou "not" ou "dsub"
        super(fenetreAjout, self).__init__(parent)
        self.setFixedSize(220, 100)
        self.layout = qtw.QVBoxLayout()
        self.control = control
        self.add_sub = add_sub
        matiere = self.control.getNameSub()

        self.matiereText = qtw.QLabel("Matière")
        self.layout.addWidget(self.matiereText)

        if not self.add_sub:
            self.setFixedSize(220, 200)
                #Création de l'interface de sélection d'une matière
            self.buttonMat = qtw.QToolButton(self)
            self.buttonMat.setPopupMode(qtw.QToolButton.MenuButtonPopup)
            self.buttonMat.setFixedSize(200, 30)
            self.buttonMat.setText(matiere[0])
            self.buttonMat.setMenu(qtw.QMenu(self.buttonMat))
            self.listMat = qtw.QListWidget(self)
            cptBuffer = 1 # Compteur à 1 pour que ça colle avec la table montrée à l'utilisateur
            for i in matiere:
                self.listMat.addItem(qtw.QListWidgetItem(i))
                cptBuffer += 1
            self.listMat.itemClicked.connect(lambda item : self.itemChanged(item))
            action = qtw.QWidgetAction(self.buttonMat)
            action.setDefaultWidget(self.listMat)
            self.buttonMat.menu().addAction(action)

                #Création du widget d'entrée des notes
            self.noteText = qtw.QLabel("Note")
            self.noteEdit = qtw.QLineEdit()

                #Création du layout pour les widgets précédents
            self.layout.addWidget(self.buttonMat)
            self.layout.addWidget(self.noteText)
            self.layout.addWidget(self.noteEdit)

        else:
            self.setFixedSize(220, 200)
                #Création du widget d'entrée de la matière
            self.matiereEdit = qtw.QLineEdit()

                #Création du widget d'entrée de la moyenne visée
            self.matierePronoText = qtw.QLabel("Moyenne visée")
            self.matierePronoEdit = qtw.QLineEdit("10") #Le paramètre par défaut de la fonction

                #Création du layout des widgets précédents
            self.layout.addWidget(self.matiereEdit)
            self.layout.addWidget(self.matierePronoText)
            self.layout.addWidget(self.matierePronoEdit)

            #Création du widget d'entrée du coefficient
        self.coeffText = qtw.QLabel("Coefficient")
        self.coeffEdit = qtw.QLineEdit()

            #Créer le bouton de confirmation
        self.accept = qtw.QPushButton("Ajouter")
        self.accept.clicked.connect(self.accepted)

         #Créer le layout générique du popup
        self.layout.addWidget(self.coeffText)
        self.layout.addWidget(self.coeffEdit)
        self.layout.addWidget(self.accept)
        self.setLayout(self.layout)

    @Slot()
    def accepted(self):
        if not self.add_sub: #On vérifie quoi ajouter
            try:
                self.control.addNotes(self.buttonMat.text(), float(self.noteEdit.text()), float(self.coeffEdit.text()))
            except Exception as e:
                popupBox(info_text = str(e), title = "Error", modal = True)
        else:
            try:
                self.control.addSub(self.matiereEdit.text(), float(self.coeffEdit.text()), float(self.matierePronoEdit.text()))
            except Exception as e:
                popupBox(info_text = str(e), title = "Error", modal = True)

        self.control = None
        self.close()

    @Slot()
    def itemChanged(self, itemCl):
        self.buttonMat.setText(itemCl.text())
        self.buttonMat.menu().hide()

class fenetreSuppr(qtw.QDialog):
    """Classe gérant les inputs dans le cas de la suppression d'une note"""
    def __init__(self, textLabel, control, del_sub = False, parent=None):
        super(fenetreSuppr, self).__init__(parent)

        self.setFixedSize(220, 100)
        self.layout = qtw.QVBoxLayout()
        self.control = control #L'objet de la bibliothèque Notes. On adopte une démarche assez 'décentralisée'
        matiere = self.control.getNameSub()
        self.del_sub = del_sub #Paramètre système; Destruction d'une note ou d'une matière ?

            #Bouton de confirmation
        self.accept = qtw.QPushButton("Ok")
        self.accept.clicked.connect(self.accepted)

            #Label de l'index
        if not self.del_sub: #On ne montre le menu des index de la matière que si on veut supprimer une note
            self.setFixedSize(220, 200)
            self.indexText = qtw.QLabel(textLabel[1])
            self.buttonInd = qtw.QToolButton(self)
            self.buttonInd.setPopupMode(qtw.QToolButton.MenuButtonPopup)
            self.buttonInd.setFixedSize(200, 30)
            self.buttonInd.setText("")
            self.buttonInd.setMenu(qtw.QMenu(self.buttonInd))
            self.listNotes = qtw.QListWidget(self)
            cptBuffer = 1 #Compteur à 1 pour que ça colle avec la table montrée à l'utilisateur
            for i in self.control.dictNotes[matiere[0]][0]:
                self.listNotes.addItem(qtw.QListWidgetItem("{} | {} - {}".format(str(cptBuffer), str(i[0]), str(i[1]))))
                cptBuffer += 1
            self.listNotes.itemClicked.connect(lambda item : self.itemMarkChanged(item))
            action = qtw.QWidgetAction(self.buttonInd)
            action.setDefaultWidget(self.listNotes)
            self.buttonInd.menu().addAction(action)

            #Menu des matières
        self.menuText = qtw.QLabel(textLabel[0])
        self.buttonMat = qtw.QToolButton(self)
        self.buttonMat.setPopupMode(qtw.QToolButton.MenuButtonPopup)
        self.buttonMat.setFixedSize(200, 30)
        self.buttonMat.setText(matiere[0])
        self.buttonMat.setMenu(qtw.QMenu(self.buttonMat))
        self.textBox = qtw.QListWidget(self)
        for i in matiere:
            self.textBox.addItem(qtw.QListWidgetItem(i))
        self.textBox.itemClicked.connect(lambda item : self.itemMatChanged(item))
        action = qtw.QWidgetAction(self.buttonMat)
        action.setDefaultWidget(self.textBox)
        self.buttonMat.menu().addAction(action)

            #On définit le layout de la fenêtre
        self.layout.addWidget(self.menuText)
        self.layout.addWidget(self.buttonMat)
        if not self.del_sub : self.layout.addWidget(self.indexText)
        if not self.del_sub : self.layout.addWidget(self.buttonInd)
        self.layout.addWidget(self.accept)
        self.setLayout(self.layout)

    @Slot()
    def accepted(self):
        if not self.del_sub: #On vérifie quoi supprimer
            index = int(self.buttonInd.text()[0]) - 1 #On soustrait 1 car on repasse à l'index des listes; et donc l'index de l'ordinateur démarrant à 0
            try:
                self.control.delNotes(self.buttonMat.text(), index)
            except Exception as e:
                popupBox(info_text = str(e), title = "Error", modal = True)
        else:
            try:
                self.control.delSub(self.buttonMat.text())
            except Exception as e:
                popupBox(info_text = str(e), title = "Error", modal = True)

        self.control = None
        self.close()

    @Slot()
    def itemMarkChanged(self, itemCl):
        self.buttonInd.setText(itemCl.text())
        self.buttonInd.menu().hide()

    @Slot()
    def itemMatChanged(self, itemCl):
        self.buttonMat.setText(itemCl.text())
        self.buttonMat.menu().hide()
        if not self.del_sub:
            self.listNotes.clear()
                #On actualise la liste des notes
            for i in self.control.dictNotes[itemCl.text()][0]: self.listNotes.addItem(qtw.QListWidgetItem("{} | {}".format(str(i[0]), str(i[1]))))

class popupBox(qtw.QMessageBox):
    """A popup class for all the messages"""
    def __init__(self, info_text, title = "Information", icon = qtw.QMessageBox.Warning, modal = False):
        """ - icon : Type of icon for the popup (specified on the qt site), should be QtWidget.QMessageBox"""
        super(popupBox, self).__init__()
        self.retour = None
        self.setIcon(icon)
        if icon is qtw.QMessageBox.Icon.Question:
            self.setStandardButtons(qtw.QMessageBox.Yes | qtw.QMessageBox.No)
            self.buttonClicked.connect(self.rep)
        self.setWindowTitle(title)
        self.setModal(modal)
        self.setText(info_text)
        self.exec()#self.output.values = [str(e)]

    def rep(self, i):
        self.retour = i.text()[1:] != "No"

class SettingsForm(qtw.QDialog):
    def __init__(self, parent = None, cryptChecked = True, syncChecked = False):
        super(SettingsForm, self).__init__(parent)
        self.setFixedSize(300, 170)
        # Administrative attributes
        self.crypted = cryptChecked
        self.sync = syncChecked
        # Create widgets
        self.checkCrypt = qtw.QCheckBox("Fichiers cryptés")
        self.checkCrypt.setChecked(cryptChecked)
        self.passCrypt = qtw.QLineEdit()
        self.passCrypt.setEchoMode(self.passCrypt.Password)
        self.passCrypt.setVisible(cryptChecked)
        self.passCrypt.setToolTip("Clé de cryptage")
        self.checkSync = qtw.QCheckBox("Synchronisation")
        self.checkSync.setChecked(syncChecked)

        self.accept = qtw.QPushButton("Ok")
        # Create layout and add widgets
        layout = qtw.QVBoxLayout()
        layout.addWidget(self.checkCrypt)
        layout.addWidget(self.passCrypt)
        layout.addWidget(self.checkSync)
        layout.addWidget(self.accept)

        # Set dialog layout
        self.setLayout(layout)

        # Add button signals
        self.checkCrypt.toggled.connect(self.setCrypt)
        self.checkSync.toggled.connect(self.setSync)
        self.accept.clicked.connect(self.accepted)

    @Slot()
    def setCrypt(self):
        self.crypted = self.checkCrypt.isChecked()
        self.passCrypt.setVisible(self.checkCrypt.isChecked())

    @Slot()
    def setSync(self):
        self.sync = self.checkSync.isChecked()

    @Slot()
    def accepted(self):
        self.close()

class mainWinWidget(qtw.QWidget):
    """Widget central: C'est lui qui s'occupe de l'affichage des dossiers/fichiers"""
    def __init__(self, notesSys):
        super(mainWinWidget, self).__init__()
        self.control = notesSys
        self.widgetNotes = qtw.QTableWidget(self)
        self.widgetStats = qtw.QTableWidget(self)

    def affichage(self, matiere):
        layout = qtw.QHBoxLayout()
        self.affichageNotes(matiere)
        self.affichageStats(matiere)
        layout.addWidget(self.widgetNotes)
        layout.addWidget(self.widgetStats)
        self.setLayout(layout)

    def affichageNotes(self, matiere):
            #Affichage des notes
        self.widgetNotes.setEditTriggers(qtw.QAbstractItemView.NoEditTriggers)
        self.widgetNotes.setRowCount(len(self.control.dictNotes[matiere][0]))
        self.widgetNotes.setColumnCount(2)
        self.widgetNotes.setHorizontalHeaderLabels(["Notes", "Coefficients"])

        cmpt = 0 #C'est un peu moche, mais j'ai pas grand chose de mieux pour l'instant
        for x in self.control.dictNotes[matiere][0]:
                #On crée les items représentant les notes
            self.widgetNotes.setItem(cmpt, 0, qtw.QTableWidgetItem(str(round(x[0], 4))))
            self.widgetNotes.setItem(cmpt, 1, qtw.QTableWidgetItem(str(round(x[1], 4))))
            cmpt += 1

    def affichageStats(self, matiere):
            #Affichage des Stats
        self.widgetStats.setEditTriggers(qtw.QAbstractItemView.NoEditTriggers)
        self.widgetStats.setRowCount(5)
        self.widgetStats.setColumnCount(1)
        self.widgetStats.setHorizontalHeaderLabels(["Statistiques"])
        self.widgetStats.setVerticalHeaderLabels(["Moyenne", "1e Quartile", "Médiane", "2e Quartile", "Ecart-type"])
            #On vérifie qu'on ne divise jamais pas 0
        if len(self.control.dictNotes[matiere][0]) != 0:
                #On donne les statistiques liées aux notes
                #On en profite pour arrondir à 5 Ch. Sign.
            self.widgetStats.setItem(0, 0, qtw.QTableWidgetItem(str(round(self.control.mean([matiere])[matiere], 4))))
            buff = self.control.percentile([25, 50, 75], [matiere])[matiere]
            self.widgetStats.setItem(1, 0, qtw.QTableWidgetItem(str(round(buff[0][1], 4))))
            self.widgetStats.setItem(2, 0, qtw.QTableWidgetItem(str(round(buff[1][1], 4))))
            self.widgetStats.setItem(3, 0, qtw.QTableWidgetItem(str(round(buff[2][1], 4))))
            self.widgetStats.setItem(4, 0, qtw.QTableWidgetItem(str(round(self.control.ecartType([matiere])[matiere], 4))))

        else:
            self.widgetStats.setItem(0, 0, qtw.QTableWidgetItem(""))
            self.widgetStats.setItem(2, 0, qtw.QTableWidgetItem(""))

class mainWinMar(qtw.QMainWindow):
    def __init__(self):
        super(mainWinMar, self).__init__()
        identification = 0
        while identification >= 0:
            id = nt.sci.IdentificationForm(self)
            id.exec_()
            if not nt.Notes.verifUser(id.username.text().strip()):
                conf = popupBox("Voulez vous créer un nouvel utilisateur?", "Utilisateur Inconnu", qtw.QMessageBox.Question, True)

                if conf.retour:
                    identification = -1
                    self.notes = nt.Notes(id.username.text().strip(), id.password.text().strip())
                else:
                    identification += 1
            else:
                self.notes = nt.Notes(id.username.text().strip(), id.password.text().strip())
                identification = -1
                #except Exception as e:
                #    print(e)
                #    badpass = qtw.QMessageBox(qtw.QMessageBox.Warning, "Authentification", "Mot de Passe faux")
                #    badpass.exec()
                #    identification += 1
            if identification >= 3:
                conf = popupBox("Trop de tentatives", "Auth failed", qtw.QMessageBox.Critical, True)
                quit()
        self.sync = self.notes.settings["Sync"]
        if self.sync is True:
            self.syncSrp = nt.sci.transferWindows()
            self.syncSrp.get()

        self.matiereStack = None
        self.srpCreds = {"Username" : None, "Password" : None}
            #Création des widgets
        self.matieresAffi = None
        self.notesAffi = None
        self.widget = mainWinWidget(self.notes)

            #Création de la fenêtre et de son pourtour
        self.window().setWindowTitle("GdN. A v1.0") #Gestionnaire de Notes Atlantica
        self.setBaseSize(500, 500)
        self.setMinimumSize(500, 500)
        self.mainMenu = self.menuBar()
            #Création d'un menu
        self.menu = [None, None, None, None]
        self.menu[0] = self.mainMenu.addMenu("Matières")
        self.menu[1] = self.mainMenu.addMenu("Edition")
        self.menu[2] = self.mainMenu.addMenu("Transfert")
        self.menu[3] = self.mainMenu.addMenu("Outils")

            #Création des sous-menus pour l'édition
        connect_action = [None for i in range(6)]
        connect_action[0] = qtw.QAction("Sauvegarder", self)
        connect_action[0].setIcon(qtg.QIcon("graphics/save2.svg"))
        connect_action[0].triggered.connect(self.save)
        connect_action[1] = qtw.QAction("Graphique", self)
        connect_action[1].setIcon(qtg.QIcon("graphics/graph2.svg"))
        connect_action[1].triggered.connect(self.show_graph)
        connect_action[2] = qtw.QAction("Ajout Note", self)
        connect_action[2].triggered.connect(self.add_mark)
        connect_action[3] = qtw.QAction("Ajout Matière", self)
        connect_action[3].triggered.connect(self.add_sub)
        connect_action[4] = qtw.QAction("Suppr. Note", self)
        connect_action[4].triggered.connect(self.del_mark)
        connect_action[5] = qtw.QAction("Suppr. Matière", self)
        connect_action[5].triggered.connect(self.del_sub)
        for i in range(len(connect_action)):
            self.menu[1].addAction(connect_action[i])

            #Création des sous-menus pour les Transferts
        connect_action = [None, None, None]
        connect_action[0] = qtw.QAction("Parcourir", self)
        connect_action[0].setIcon(qtg.QIcon("graphics/search.svg"))
        connect_action[0].triggered.connect(self.srpBrowse)
        connect_action[1] = qtw.QAction("Envoyer", self)
        connect_action[1].setIcon(qtg.QIcon("graphics/send.svg"))
        connect_action[1].triggered.connect(self.srpSend)
        connect_action[2]  = qtw.QAction("Récupérer", self)
        connect_action[2].setIcon(qtg.QIcon("graphics/download.svg"))
        connect_action[2].triggered.connect(self.srpRetr)

        for i in range(len(connect_action)):
            self.menu[2].addAction(connect_action[i])
            #Création des sous-menus pour les Paramètres
        connect_action = [None, None]
        connect_action[0] = qtw.QAction("Paramètres", self)
        connect_action[0].setIcon(qtg.QIcon("graphics/settings.svg"))
        connect_action[0].triggered.connect(self.settings)
        connect_action[1] = qtw.QAction("SRP Settings", self)
        connect_action[1].setIcon(qtg.QIcon("graphics/logo-srp.png"))
        connect_action[1].triggered.connect(self.srpSettings)
        for i in range(len(connect_action)):
            self.menu[3].addAction(connect_action[i])

            #Création d'une toolbar
        self.mainToolBar = qtw.QToolBar(self)
        self.addToolBar(Qt.ToolBarArea.LeftToolBarArea, self.mainToolBar)
        self.menuMatieres()

            #Création d'un layout
        self.layout = qtw.QGridLayout()
        self.setLayout(self.layout)

    def __del__(self):
        self.notes.sauvegarde()
        if self.sync:
            self.syncSrp.send()

    def menuMatieres(self):
        self.matieresAffi = {}
        self.mainToolBar.clear()
        for mat in self.notes.dictNotes.keys():
            actionBuffer = qtw.QAction(mat)
            actionBuffer.setCheckable(True)
            actionBuffer.triggered.connect(lambda checked = False, v=mat : self.tab(v))
            self.mainToolBar.addAction(actionBuffer)
            self.matieresAffi[mat] = actionBuffer

    @Slot()
    def settings(self):
        if len(self.notes.settings.keys()) != 0:
            settings = SettingsForm(cryptChecked = self.notes.settings["Crypt"], syncChecked = self.notes.settings["Sync"])
        else:
            settings = SettingsForm()
        settings.exec_()
        self.notes.password = settings.passCrypt.text()
        print(settings.passCrypt.text())
        self.notes.settingsVerif(True, {"Crypt" : settings.crypted, "Sync" : settings.sync})

    @Slot()
    def srpSettings(self):
        conf = nt.sci.configWindow(self.notes.folder, self)
        conf.exec_()

    @Slot()
    def srpBrowse(self):
        if self.sync:
            self.srpCreds["Username"] = self.syncSrp.username
            self.srpCreds["Password"] = self.syncSrp.password

        elif None in self.srpCreds.values():
            idSrp = nt.sci.IdentificationForm(self, qtg.QIcon("graphics/logo-srp.png"))
            idSrp.exec_()
            self.srpCreds["Username"] = idSrp.username.text()
            self.srpCreds["Password"] = idSrp.password.text()
        try:
            brSrp = nt.sci.browseWindow(self.notes.folder, self.srpCreds["Username"], self.srpCreds["Password"], self)
            brSrp.exec_()
        except nt.sci.srp.exceptionSSH as e:
            self.srpCreds = {"Username" : None, "Password" : None}
            popupBox(info_text = str(e), title = "Error SRP", icon = qtw.QMessageBox.Critical, modal = True)

    @Slot()
    def srpSend(self):
        pass

    @Slot()
    def srpRetr(self):
        pass

    @Slot()
    def tab(self, mat):
        self.matieresAffi[mat].setChecked(True)
        for i in self.matieresAffi.keys():
            if i is not mat:
                self.matieresAffi[i].setChecked(False)
        self.widget.affichage(mat)
        self.setCentralWidget(self.widget)
        self.matiereStack = mat

    @Slot()
    def show_graph(self):
        for i in self.matieresAffi.keys():
            if self.matieresAffi[i].isChecked():
                sub = i
        try:
            test = self.notes.courbe_tendance(sub)
        except UnboundLocalError as e:
            print("\033[31m" + str(e) + "\033[0m")
            popupBox(info_text = "Pas de matière sélectionnée", title = "Error", modal = True)
        widget = qtw.QDialog()
        widget.setWindowTitle(sub)
        layout = qtw.QVBoxLayout()
        layout.addWidget(test)
        widget.setLayout(layout)
        widget.exec_()



    @Slot()
    def add_mark(self):
        demande = fenetreAjout(self.notes, False, self)
        demande.exec_()
        self.tab(demande.buttonMat.text())

    @Slot()
    def add_sub(self):
        demande = fenetreAjout(self.notes, True, self)
        demande.exec_()
        self.menuMatieres()

    @Slot()
    def del_sub(self):
        demande = fenetreSuppr(["Matières"], self.notes, True, self)
        demande.exec_()
        self.menuMatieres()

    @Slot()
    def del_mark(self):
        demande = fenetreSuppr(["Matière", "Index Note"], self.notes, False, self)
        demande.exec_()
        self.tab(demande.buttonMat.text())

    @Slot()
    def save(self):
        self.notes.sauvegarde()

if __name__ == '__main__':
    # Create the Qt Application
    app = qtw.QApplication(sys.argv)
    # Create and show the form
    form = mainWinMar()
    form.show()
    del form
    # Run the main Qt loop
    sys.exit(app.exec_())
