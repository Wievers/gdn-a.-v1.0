import sys, time
from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QObject
from PySide2 import QtGui as qtg

import srplib2 as srp

ORIG_PATH = srp.os.getcwd()

class LoadingForm(qtw.QProgressDialog):
    def __init__(self, host = None, title = "Progress", labelText = "Test", cancelButtonText = "Cancel"):
        # QProgressDialog::QProgressDialog(const QString &labelText, const QString &cancelButtonText, int minimum, int maximum, QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags())
        super(LoadingForm, self).__init__(title, cancelButtonText, 0, 99) #En pourcentage pour ma part
            #Parametres graphiques
        self.setWindowFlags(Qt.WindowTitleHint | Qt.Dialog | Qt.WindowMaximizeButtonHint | Qt.CustomizeWindowHint)
        self.setWindowTitle(title)
        self.setWindowModality(Qt.WindowModal)
            #Paramètres interface

    def actionProgress(self, sent, filename):
        filename = str(filename).strip("'")
        self.setValue(sent)
        self.setLabelText(filename)

class IdentificationForm(qtw.QDialog):
    """This class creates the form that will be generated to get the credentials of the user
        - icon : a qt Icon for the window"""
    def __init__(self, parent = None, icon = None):
        super(IdentificationForm, self).__init__(parent)
        # Set windows properties
        self.setFixedSize(300, 150)
        self.setWindowTitle("Authentication")
        if icon is not None:
            self.setWindowIcon(icon)
        # Create widgets
        self.unText = qtw.QLabel("Username:")
        self.username = qtw.QLineEdit("")
        self.pwdText = qtw.QLabel("Password:")
        self.password = qtw.QLineEdit("")
        self.password.setEchoMode(self.password.Password)
        self.accept = qtw.QPushButton("Ok")
        # Create layout and add widgets
        layout = qtw.QVBoxLayout()
        layout.addWidget(self.unText)
        layout.addWidget(self.username)
        layout.addWidget(self.pwdText)
        layout.addWidget(self.password)
        layout.addWidget(self.accept)
        # Set dialog layout
        self.setLayout(layout)
        # Add button signal to greetings slot
        self.accept.clicked.connect(self.accepted)

    @Slot()
    def accepted(self):
        self.close()

class configWindow(qtw.QDialog):
    def __init__(self, folderProg_name, parent = None):
        super(configWindow, self).__init__(parent)
        self.setFixedSize(300, 250)
        self.setWindowTitle("SRP Settings")
        self.setWindowIcon(qtg.QIcon("graphics/logo-srp.png"))
        self.folderName = folderProg_name

        self.listWidgetsConf = [None for i in range(6)]
        self.listWidgetsConf[0]  = qtw.QLabel('<p style="color:red;">Local Root Directory : </p>')
        self.listWidgetsConf[1] = qtw.QPushButton("Browse Files")
        self.listWidgetsConf[1].setToolTip("The local directory where will be store the files/folders retrieved with the SRP")
        self.listWidgetsConf[1].clicked.connect(self.browse)
        #self.Help1     = qtw.QLabel(values=["Relative path: The SRP will directly create a full path accordingly (in your home by default)",
        #            "Full Path: The SRP will store it directly but will send back an error if it doesn't exist",
        #            "Either way, the given directory shall an existing directory"])
        self.listWidgetsConf[2]   = qtw.QLabel('<p style="color:red;"> Server Root Directory : </p>')
        self.listWidgetsConf[3] = qtw.QLineEdit()
        self.listWidgetsConf[3].setToolTip("The server directory where will be store the files/folders sent with the SRP")
        self.listWidgetsConf[4]   = qtw.QLabel('<p style = "color:red;"> Server IP/DynDNS address : </p>')
        self.listWidgetsConf[5] = qtw.QLineEdit()
        self.listWidgetsConf[5].setToolTip("The address of the server where your files will be stored")

        self.depthTitle    = qtw.QLabel('<p style = "color:red;"> Maximum Recursion Depth : </p>')
        self.depthInd     = qtw.QLineEdit()
        self.depthInd.setValidator(qtg.QIntValidator(0, 100, self))
        self.depthInd.setToolTip("How deep will navigate the SRP through the directory arborescence")

        self.layoutDepth = qtw.QHBoxLayout()
        self.layoutDepth.addWidget(self.depthTitle)
        self.layoutDepth.addWidget(self.depthInd)

        self.apply = qtw.QPushButton("Apply")
        self.apply.clicked.connect(self.applySets)


        self.layout = qtw.QVBoxLayout()

        for i in self.listWidgetsConf:
            self.layout.addWidget(i)
        self.layout.addLayout(self.layoutDepth)
        self.layout.addWidget(self.apply)
        self.setLayout(self.layout)

    def applySets(self):
        conf = srp.ServInteract.configuration()
        conf.configFileServer(self.listWidgetsConf[3].text(), self.folderName)
        conf.configFileClient(self.listWidgetsConf[1].text())
        conf.configAddressServer(self.listWidgetsConf[5].text())
        conf.configRecursion(self.depthInd.text())
        conf.writeConfiguration()
        self.close()

    def browse(self):
        files = qtw.QFileDialog()
        files.setOption(qtw.QFileDialog.ShowDirsOnly, True)
        files.setFileMode(qtw.QFileDialog.DirectoryOnly)
        files.exec_()
        self.listWidgetsConf[1].setText(files.directory().absolutePath())

class browseWindow(qtw.QDialog):
    def __init__(self, folderProg_name, srp_username = None, srp_password = None, parent = None):
        super(browseWindow, self).__init__(parent)
         #Adm
        self.setFixedSize(300, 250)
        self.setWindowTitle("SRP Files")
        self.setWindowIcon(qtg.QIcon("graphics/logo-srp.png"))

         #SRP adm
        if srp_password is None or srp_username is None:
            cred = IdentificationForm(self, qtg.QIcon("graphics/logo-srp.png"))
            cred.exec_()
            self.srp = srp.ServInteract(cred.username.text(), cred.password.text())
        else:
            self.srp = srp.ServInteract(srp_username, srp_password)
        self.srp.allFilesList()

         #Widg
        self.localFiles = qtw.QTreeWidget()
        self.localFiles.setHeaderLabels(["Local Files"])
        self.remoteFiles = qtw.QTreeWidget()
        self.remoteFiles.setHeaderLabels(["Remote Files"])
        try:
            self.addItemsTree(self.srp.localCompFiles.partialDictFiles, self.localFiles)
            self.addItemsTree(self.srp.remoteCompFiles.partialDictFiles, self.remoteFiles)
        except Exception as e:
            print("\033[31m" + "Error : " + "\033[0m"+ str(e))
         #Layouts
        self.layout = qtw.QHBoxLayout()
        self.layout.addWidget(self.localFiles)
        self.layout.addWidget(self.remoteFiles)
        self.setLayout(self.layout)

    def addItemsTree(self, elements, datasTree):
        for x in elements:
            if x["isFile"] is True:
                newDT = qtw.QTreeWidgetItem(datasTree, [x["Name"]])
            else:
                newDT = qtw.QTreeWidgetItem(datasTree, [x["Name"]])
                self.addItemsTree(x["Files"], newDT)

class transferWindows:
    def __init__(self, srp_username = None, srp_password = None):
        if srp_password is None or srp_username is None:
            cred = IdentificationForm(icon = qtg.QIcon("graphics/logo-srp.png"))
            cred.exec_()
            self.username = cred.username.text()
            self.password = cred.password.text()
            self.srp = srp.ServInteract(self.username, self.password)
        else:
            self.username = srp_username
            self.password = srp_password
            self.srp = srp.ServInteract(self.username, self.password)

    def get(self):
        loadingPop = LoadingForm("Download", "Synchronization : Retrieving files...")
        try:
            self.srp.fileDownloaderSRP(lambda a, b : loadingPop.actionProgress(a, b)) #Retrieving the files and keeping tack of the progress
        except Exception as e:
            print("\033[31m" + "Error : " + "\033[0m" + str(e))
        srp.os.chdir(ORIG_PATH)

    def send(self):
        loadingPop = LoadingForm("Upload", "Synchronization : Sending files...")
        try:
            self.srp.fileSaverSRP(lambda a, b : loadingPop.actionProgress(a, b)) #Sending the files and keeping track of the progress
        except Exception as e:
            print("\033[31m" + "Error : " + "\033[0m" + str(e))
        srp.os.chdir(ORIG_PATH)
