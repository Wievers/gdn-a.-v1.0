# AES 256 encryption/decryption using pycrypto library

import base64
import hashlib
from Crypto.Cipher import AES
from Crypto import Random
import getpass

BLOCK_SIZE = 16
#pad = lambda s: s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * chr(BLOCK_SIZE - len(s) % BLOCK_SIZE)
unpad = lambda s: s[:-ord(s[len(s) - 1:])]
def getPass():
    return getpass.getpass("Enter encryption password: ")

class fichierCrypt:
    def __init__(self, mdp, fichierBase = "textEncrypt"):
        self.fichier = fichierBase
        self.password = mdp
        self.block_size = 16

    def _pad(self, s):
        # pad with bytes instead of str
        return s + (BLOCK_SIZE - len(s) % BLOCK_SIZE) * \
        chr(BLOCK_SIZE - len(s) % BLOCK_SIZE).encode('utf8')

    def encrypt(self, raw):
        private_key = hashlib.sha256(self.password.encode("utf-8")).digest()
        raw = raw.encode("utf-8")
        raw = self._pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        save_file = open(self.fichier, "wb")
        save_file.write(base64.b64encode(iv + cipher.encrypt(raw)))
        save_file.close()


    def decrypt(self):
        save_file = open(self.fichier, "rb")
        enc = save_file.read()
        private_key = hashlib.sha256(self.password.encode("utf-8")).digest()
        enc = base64.b64decode(enc)
        iv = enc[:16]
        cipher = AES.new(private_key, AES.MODE_CBC, iv)
        save_file.close()
        try:
            return bytes.decode(unpad(cipher.decrypt(enc[16:])))
        except:
            return ""


# First let us encrypt secret message
'''
ecr = input("Read (1) or Write (0): ")
test = fichierCrypt(mdp = password)
if int(ecr) == 0:
    str = getpass.getpass("Enter message: ")
    test.encrypt(str)
# Let us decrypt using our original password
elif int(ecr) == 1:
    decrypted = test.decrypt()
    print(decrypted)
'''
