import numpy as np
import os
import os.path as pth
import sys
import json as js

import cryptTest as cr
import SrpConfigInterface as sci

import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

#print("Bienvenue dans le programme de notes statistiques")
#matière\/notes:coeff&
""" [01] : Bug sur mean"""
class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)

class Notes:
    folder = "nt-gdna"
    settingFile = "configgdna.cfg"
    def __init__(self, user, mdp):
        self.settings = None
        self.settingsVerif()
        self.password = mdp
        self.fichier = self.fileVerification(user)
        self.dictNotes = {}

        if pth.isfile(self.fichier):
            self.load()

      #Fonction d'administration
    @classmethod
    def verifUser(cls, user):
        return (pth.isfile(pth.join(cls.folder, user + "_cr")) or pth.isfile(pth.join(cls.folder, user + "_cl")))

    def settingsVerif(self, write = False, newSettings = {}):
        if not write:
            if not pth.isfile(self.settingFile):
                config = open(self.settingFile, "w")
                if newSettings is not {}:
                    config.write(js.dumps({"Crypt" : True, "Sync" : False}))
                    self.settingsVerif()
            else:
                configRead = open("configgdna.cfg", "r")
                data = configRead.read()
                self.settings = js.loads(data)
                configRead.close()

        else:
            config = open(self.settingFile, "w")
            if newSettings is not {}:
                config.write(js.dumps(newSettings))
                self.settings = newSettings
            else:
                config.write(js.dumps(self.settings))
            config.close()

    def fileVerification(self, fileName):
        if not pth.isdir(self.folder): os.makedirs(self.folder)
        if len(self.settings.keys()) != 0:
            if not self.settings["Crypt"]:
                return pth.join(self.folder, fileName + "_cl") #Le fichier est crypté
            else:
                return pth.join(self.folder, fileName + "_cr")
        else:
            return pth.join(self.folder, fileName + "_cr")

    def load(self):
        if self.fichier[:-3] != -1:
            myMarks = open(self.fichier, "r")
            marksBuffer = myMarks.read()
            try:
                if len(marksBuffer) != 0: self.dictNotes = eval(marksBuffer)
                else: self.dictNotes = {}
            except Exception as e:
                raise e
            myMarks.close()
        else:
            myMarks = cr.fichierCrypt(self.password, self.fichier)
            if len(myMarks.decrypt()) == 0:
                raise Exception("Wrong password")
            else:
                try:
                    myMarks = eval(myMarks.decrypt())
                except Exception as e:
                    raise e
            self.dictNotes = myMarks


    def sauvegarde(self):
        if self.settings["Crypt"]:
            noteFile = cr.fichierCrypt(self.password, self.fichier)
            noteFile.encrypt(str(self.dictNotes))
            os.rename(self.fichier, self.fichier[:-3] + "_cr")
        else:
            noteFile = open(self.fichier, "w")
            noteFile.write(str(self.dictNotes))
            os.rename(self.fichier, self.fichier[:-3] + "_cl")

      #Fonctions de calcul
    def courbe_tendance(self, nomSub):
        coeffToPlot = []
        for i in self.dictNotes[nomSub][0]:
            coeffToPlot.append(i[0])
        sc = MplCanvas(self, width = 5, height = 4, dpi = 100)
        sc.axes.plot([float(i) for i in range(len(coeffToPlot))], [float(j) for j in coeffToPlot], label = "Notes", color = "r")
        sc.axes.scatter([float(i) for i in range(len(coeffToPlot))], [float(i) for i in coeffToPlot], color = "r")
        return sc

    def flush(self):
        if pth.isfile(self.fichier):
            os.remove(self.fichier)
        else:
            pass

    def content(self):
        return self.dictNotes

    def getNameSub(self):
        buffer = []
        for i in self.dictNotes.keys():
            buffer.append(i)
        return buffer

    def getContentSub(self, nomSub):
        try:
            return self.dictNotes[nomSub]
        except Exception as e:
            raise e

    def addSub(self, nomSub, coeffSub, obj = 10, noteCoeff = None):
        """Fonction ajoutant une matière
            nomSub : Nom de la matière
            coeffSub : Coefficient de la matière
            obj : Moyenne attendue pour la matière (facultatif, par défaut 10)
            noteCoeff : Liste de tuple de notes et de leurs coefficients (facultatif)"""
        if noteCoeff is None: noteCoeff = []
        self.dictNotes[nomSub] = (noteCoeff, coeffSub, obj)
        self.sauvegarde()

    def delSub(self, nomSub):
        """Fonction supprimant une matière
            nomSub : Nom de la maitière à supprimer"""
        try:
            self.dictNotes.pop(nomSub)
            self.sauvegarde()
        except Exception as e:
            raise(e)

    def addNotes(self, matiere, note, coefficient):
        if type(note) is not float and type(coefficient) is not float:
            raise TypeError("{} or/and {} is not a float".format(str(type(note)), str(type(coefficient))))
        if type(matiere) is None:
            raise TypeError("{} is not str".format(str(type(matiere))))
        if matiere in self.dictNotes.keys():
            self.dictNotes[matiere][0].append((note, coefficient))
        else:
            raise Exception("Matière n'existe pas")
        self.sauvegarde()

    def delNotes(self, matiere, noteIndex):
        if matiere in self.dictNotes.keys():
            self.dictNotes[matiere][0].pop(noteIndex)
            self.sauvegarde()
        else:
            raise Exception("Note n'existe pas")

    def mean(self, listMat = None):
        if listMat == None:
            listMat = self.dictNotes.keys()
        moy = {}

        for sub in listMat:
            meanBuff = []
            for mark in self.dictNotes[sub][0]:
                for i in range(int(mark[1])):
                    meanBuff.append(mark[0])

            if len(meanBuff) != 0:
                moy[sub] = np.mean(meanBuff)
            else:
                raise noteException("[01] Liste vide")
        return moy

    def percentile(self, wantedQuart, listMat = None):
        if listMat is None:
            listMat = self.dictNotes.keys()

        perc = {}

        for sub in listMat:
            medBuff = []
            for mark in self.dictNotes[sub][0]:
                for i in range(int(mark[1])):
                    medBuff.append(mark[0])
            medBuff.sort()

            buffPerc = []
            try:
                for i in wantedQuart:
                    buffPerc.append((i, np.percentile(medBuff, i)))
                perc[sub] = buffPerc

            except IndexError:
                raise noteException("Liste nulle : ".format(str(sub)))

        return perc

    def ecartType(self, listMat = None):
        if listMat is None: listMat = self.dictNotes.keys()
        dictET = {}
        for sub in listMat:
            medBuff = []
            for mark in self.dictNotes[sub][0]:
                medBuff.append(mark[0])
            dictET[sub] = np.amax(medBuff) - np.amin(medBuff)
        return dictET

    def pronostic(self, nomSub, coeffNewMark):
        coeffNewMark = int(coeffNewMark)
        moySub = self.mean([nomSub])
        coeffMoy = 0
        for i in self.dictNotes[nomSub][0]:
            coeffMoy += int(i[1])

        pro = (int(self.dictNotes[nomSub][2])*(coeffMoy + coeffNewMark) - moySub[0][1] * coeffMoy)/coeffNewMark
        return pro

class noteException(Exception):
    pass
