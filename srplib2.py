import folderview as fw

import os.path as pth
import subprocess as sp
import paramiko as pm
import tarfile as tar
import json as js
import platform
import scp
import sys
import os

RED = "\033[31m"
GREEN = "\033[32m"
YELLOW = "\033[33m"
BLUE = "\033[34m"
CYAN = "\033[36m"
NEUTRAL = "\033[0m"
ERROR = "ERROR : "
WARNING = YELLOW + "WARNING : " + NEUTRAL
pwdBasic = os.getcwd()

class connectionProtocol:
    """This Class manages all the dialog protocols needed with a remote computer.
            - Creation of SSH connection
            - Downloading and uploading files on a remote computer using SCP protocol
            - List directories on the given remote computer (in the SRP folder)i
        It may not be use outside this file or it'll be really hard to intricate everything in there"""

    def __init__(self, username, password, address):
        self.ipId = address
        self.unId = username
        self.pwdId = password
        self.sshConn = pm.SSHClient()
        self.sftpConn = None

        self.progressBuffer = 0
        self.progbuff2 = 0

    def progress(self, sent, useless, show_prog, sizeAllSend, singleSendSize, filename):
        """Show the progress of a task :
            - sent : A variable send by the Paramiko Library
            - useles : A variable used by the Paramiko Library
            - showProg : The callback function we will use
            - sizeAllSend : The size of all the files to send
            - singleSendSize : The size of the file currenctly being send
            - filename : The name of the file currently being send"""
        #if sent >= 0 and sent <= :
        sent += singleSendSize
        show_prog(int((sent*100)/sizeAllSend), filename)

    def sshConnection(self):
        """This method creates an SSH connection to a remote server"""
            #SSH connection establishment

        self.sshConn.set_missing_host_key_policy(pm.client.AutoAddPolicy())
        self.sshConn.load_system_host_keys()
        try:
            self.sshConn.connect(self.ipId, username = self.unId, password = self.pwdId)
            self.sftpConn = pm.SFTPClient.from_transport(self.sshConn.get_transport())
        except pm.ssh_exception.AuthenticationException: #Auth error
            raise exceptionSSH("Authentication error : when attempting to connect to the remote server")
        except Exception as e:
            raise exceptionSSH(str(e))

    def listRemoteFolders(self, filename, depth = 4):
        """ This method is listing (recursively) the names of the files and the folders on the server and returns a list
            of them
             - filename : The path to the directory where we want to list items
             - depth : The depth of the recursion we want (By default, it's 4)
             - return outFlux : return a list of 2 lists with:
                                            1- The list with all the files folders
                                            2- The list with the folders"""
        command = "du -ad " + depth + " " + filename
        commandVerif = "du -d " + depth + " " + filename
        stdin, stdout, stderr = self.sshConn.exec_command(command)
        verifIn, verifOut, verifErr = self.sshConn.exec_command(commandVerif)
        outFlux = [[],[]] #Liste contenant tous les fichiers/Dossiers en sortie de commande

        for line in stdout:
            compt = line.find(filename)+len(filename) + 1
                #Dans un premier temps, on récupère une liste de tous les éléments sortis de la commande (fichier/dossier compris)
            outFlux[0].append(line[compt:].strip("\n"))

            #N'ayant absolument aucun moyen de vérifier si un dossier vide n'est pas un fichier, une demande au serveur pour avoir les
            #noms des dossiers s'impose, autant la faire dans la foulée.
        for line in verifOut:
            compt = line.rfind("/") + 1  #On ne veut que les noms cette fois ci.
            outFlux[1].append(line[compt:].strip("\n"))

        outFlux[0].pop() #On sort le dernier élément, vide
        outFlux[1].pop()

        return outFlux

    def createFold(self, nameFolder, pathFold = None):
        """ This method creates a folder with it's name and it's path (or only it's name, if you give the full path with)
            on the remote computer
             - nameFolder : The name of the folder to create
             - pathFold : [OPTIONAL] The name of the path where to create the folder """
        if pathFold is not None:
            path = pth.join(pathFold, nameFolder)
        else:
            path = nameFolder
        if path == '/':
            # absolute path so change directory to root
            raise exceptionSSH("Can't create folder on the root directory '/'")
        if path == '':
            # top-level relative directory must exist
            return False
        try:
            self.sftpConn.chdir(path) # sub-directory exists
        except IOError:
            dirname, basename = os.path.split(path.rstrip('/'))
            self.createFold(dirname) # make parent directories
            self.sftpConn.mkdir(basename) # sub-directory missing, so created it
            self.sftpConn.chdir(basename)
            return True

    def retrieveFile(self, fileRetrieve, dir_local, dir_remote, fProgress):
        """This method retrieve a file from the directory specified in the config file on a remote server"""
        success = False
        sshClient = self.sshConn
        scpClient = scp.SCPClient(sshClient.get_transport(), progress = fProgress)

        stack = []
        all_size = 0
        for x in fileRetrieve:
            all_size += self.sftpConn.lstat(pth.join(dir_remote, x["Path"].strip(), x["Name"].strip())).st_size
            pathTmp = pth.join(dir_local, x["Path"].strip())
            if not pth.isdir(pathTmp) and pathTmp not in stack:
                stack.append(pathTmp)
                try:
                    os.makedirs(pathTmp)
                except Exception as e:
                    raise exceptionSSH(str(e) + " : " + dir_local)

            elif pathTmp not in stack:
                stack.append(pathTmp)

        buff_size = 0
        for x in fileRetrieve: #These two for are separated because we want ALL the folders created before getting the files (and stores them in)
            prog = lambda a, b : self.progress(a, b, fProgress, all_size, buff_size, x["Name"])
            buff_loc = pth.join(dir_local, x["Path"].strip(), x["Name"].strip())
            buff_rem = pth.join(dir_remote, x["Path"].strip(), x["Name"].strip())
            try:
                self.sftpConn.get(buff_rem, buff_loc, prog)
            except Exception as error:
                try:
                    os.remove(buff_loc)
                    self.sftpConn.get(buff_rem, buff_loc, prog)
                except scp.SCPException as error:
                    raise exceptionSSH(str(error))

    def sendFile(self, f_save, dir_local, dir_remote, fProgress): #We send the progress to the controller and the interface
        """This method save a file on the remote server at the given directory
            - f_save : The list of the files to save
            - dir_local : The local directory where to find the files to save
            - dir_remote : The remote directory where to copy the files
            - fProgress : A callback function that accepts the percentage of the advancement and the filename of the
                    file being copied currently func((int),(str))"""
            #Verifying that the program finished successfully. The request can fail but if the program handled the error (even if the task is not
            # done at the end), it's succesful. The program shall not solve an error then quit without attempting the said request.
            #On crée l'arborescence
        saveStack = []
        all_size = 0
        for x in f_save:
            all_size += os.stat(pth.join(dir_local, x["Path"].strip(), x["Name"].strip())).st_size
            pathTmp = pth.join(dir_local, x["Path"].strip())
            if len(x["Path"]) != 0 and pathTmp not in saveStack:
                self.createFold(x["Path"], dir_remote)
                saveStack.append(pathTmp)

        buff_size = 0
        for x in f_save:
            prog = lambda a, b : self.progress(a, b, fProgress, all_size, buff_size, x["Name"])
            buff_rem = pth.join(dir_remote, x["Path"].strip(), x["Name"].strip())
            buff_loc = pth.join(dir_local, x["Path"].strip(), x["Name"].strip())
            try:
                self.sftpConn.put(buff_loc, buff_rem, prog)
            except Exception as error:
                try:
                    self.sftpConn.remove(buff_rem)
                    self.sftpConn.put(buff_loc, buff_rem, prog)
                except scp.SCPException as e:
                    raise exceptionSSH(str(e))
            buff_size = buff_size + os.stat(pth.join(dir_local, x["Path"].strip(), x["Name"].strip())).st_size
            #scpClient.close()

    def isDirCmp(self, directoryToTest):
        """This method checks whether or not a directory exists
           directoryToTest : The directory you want to test"""
        try:
            comTest = "test -d " + directoryToTest + ' && echo "1" || echo "0"';
        except TypeError as e:
            raise exceptionSSH(e);

        stdin, stdout, stderr = self.sshConn.exec_command(comTest)

        if int(stdout.read()) == 1:
            return True
        else:
            return False

    def errorRemoteCmp(self, error, serverPath):
        """ This method will attempt to save the task from failing miserably. If it can't be saved, it will send back an error"""
        error = str(error)
        sshClient = self.sshConn
        if error.count("No such file or directory") != 0:
            try:
                sshClient.exec_command("mkdir "+serverPath[:serverPath.rfind('/')+1])
                return False
            except pm.ssh_exception.SSHException as fatalErr:
                raise exceptionSSH("Unexpected issue with the ssh2 protocol: \n" + fatalErr)
        return True

class exceptionSSH(Exception):
    pass

class ServInteract:
    """ This shall use the the SRP core (ssh protocol) to transfer files, gather files, and manage them on the computer and on the remote computer
        The init need the username and the password of the user who wants to log on the remote computer"""
    def __init__(self, uncred, pwdcred):
            #La question a longuement était étudiée: Faut-il demander les identifier au tout début à chaque appel
            #Du controlleur. Vu le programme, je pense qu'il vaut mieux....
       self.settings = None
       self.username = uncred.strip()
       self.password = pwdcred.strip()
       self.remoteCompFiles = None
       self.localCompFiles = None

    @staticmethod
    def configuration():
        """This method is calling the configuration interface of the SRP """
        configSRP = filesConfiguration()
        return configSRP

    def confVerification(self):
        myConf = self.configuration()
        if myConf.isConfig():
            sshConn = connectionProtocol(self.username, self.password, self.settings["Address"])
            sshConn.sshConnection()
            myConf.clientConfigCreation()
            myConf.serverConfigCreation(sshConn)
            return True
        else:
            return False

    def credUpdate(self, username, password):
        self.username = username
        self.password = password

    def fileVerification(self): #Modified
        """Retrieve the settings for the controler object, verify that all the indexes are up to date for the core methods (put/get) """
        if self.settings is None:
            self.settings = self.getConf()
            self.confVerification()

        if self.username is None: #Verifying that the username is initialized correctly
            raise exceptionSSH("Username not initialized !")

        if self.password is None: #Verifying that the password is initialized correctly
            raise exceptionSSH("Password not initialized !")

        if "" in self.settings: #Verifying the settings
            raise exceptionSSH("The settings are incomplete or corrupted, please configure the SRP")

            #We retrieve the regex of the files on the server
        self.allFilesList(True, True)
            #We retrieve the regex of the local files
        self.allFilesList(False, True)

    def allFilesList(self, remote = None, updateIndex = False): #Modified
        """ This method create/update the index of the files on ther remote computer or in the local directory
             - remote : Whether we want to update the remote directory (True), or the local directory (False)
             - updateIndex : System parameter that enable files verification"""
        if updateIndex is False: #We verify the files if they are not verified
            self.fileVerification()
            return #De facto, by verifying the files, the files list will be updated

        if remote or remote is None:
            sshConn = connectionProtocol(self.username, self.password, self.settings["Address"])
            sshConn.sshConnection()
            remoteFiles = sshConn.listRemoteFolders(self.settings["Remote"], self.settings["Recursion"])
            self.remoteCompFiles = fw.folderView(listComplete = remoteFiles[0], list_verif = remoteFiles[1])

        if not remote or remote is None:
            self.localCompFiles = fw.folderView(self.localFilesList(self.settings["Local"]))

    def localFilesList(self, filename = None, depth = 0):
        """Create a generator of the filesystem (root = SRP default directory) on the local computer """
        if filename is None:
            filename = self.settings["Local"]
        try:
            os.chdir(filename)
        except FileNotFoundError as e:
            raise exceptionSSH("Can't get to the directory: " + str(e))

        for x in os.listdir():
            if pth.isfile(pth.join(filename,x)):
                yield x
            else:
                yield x, self.localFilesList(pth.join(filename, x), depth + 1)

    def fileSaverSRP(self, fProgress, NameFilesTS = None): #Modified
        """This method have to send the requested files to the SRP
         It requires the config file
          - fProgress : The function that will show up the progress of the file transit on your interface
                        WARNING: This function will be used by the paramiko library,
                                 you may want to see the specification on gitHub: https://github.com/paramiko/paramiko
          - NameFilesTS : The list of the names of the files you want to send
          - return : The list of all the actions the function have done to succeed"""
        self.fileVerification()
        SRP = connectionProtocol(self.username, self.password, self.settings["Address"])
        SRP.sshConnection()
        returnValue = [] #This list should return the output of the function (which folder have been created, and which files have been sent
            #The main directory IS NOT the SRP directory. That's why you should NEVER forget to put the following command when working on the files stored
            #By the SRP
        try:
            os.chdir(self.settings["Local"])
        except FileNotFoundError as e:
            raise exceptionSSH("Can't get to the local directory: \n" + str(e))
            #The goal of this if statements is to tell exactly which files do we send. All of them, or part of them
        if NameFilesTS is None:
            localFiles = self.localCompFiles.findFiles(keyWord = "Path")
        else:
            localFiles = self.localCompFiles.findFiles(listName = NameFilesTS, keyWord = "Path")

        SRP.sendFile(localFiles, self.settings["Local"], self.settings["Remote"], fProgress)
        return returnValue #We could have used set to remove doublons, but the stack, while more complicated,
                            #isn't used only against doublons, but also against other absurdities

    def fileDownloaderSRP(self, fProgress, NameFilesTR = None): #Modified
        """This method have to get the requested/all files from a remote computer
            - fProgress :  The function that will show up the progress of the file transit on your interface
                        WARNING: This function will be used by the paramiko library,
                                 you may want to see the specification on gitHub: https://github.com/paramiko/paramiko
          - NameFilesTR : The list of the names of the files you want to get
          - return : The list of all the actions the function have done to succeed"""

        self.fileVerification()
        SRP = connectionProtocol(self.username, self.password, self.settings["Address"])
        SRP.sshConnection()
        returnValue = []

            #Vérification que le dossier existe
        try:
            os.chdir(self.settings["Local"])
        except FileNotFoundError + e:
            raise exceptionSSH("Can't get to the local directory: \n" + str(e))

            #On récupère les fichiers que l'on souhaite envoyer
        if NameFilesTR is None:  #We don't create empty folders here. We want to optimize the server space
            remoteFiles = self.remoteCompFiles.findFiles(keyWord = "Path")
        else:
            remoteFiles = self.remoteCompFiles.findFiles(listName = NameFilesTR, keyWord = "Path")

        SRP.retrieveFile(remoteFiles, self.settings["Local"], self.settings["Remote"], fProgress)
        return returnValue

    def research(self, element, keyWord = None, extResearch = True, server = True):
        if extResearch:
            if type(element) is list:
                if server:
                    found = self.remoteCompFiles.extSearch(list_Ext = element)
                else:
                    found = self.localCompFiles.extSearch(list_Ext = element)
            elif type(element) is str:
                if server:
                    found = self.remoteCompFiles.extSearch(key_Ext = element)
                else:
                    found = self.localCompFiles.extSearch(key_Ext = element)
            else:
                raise exceptionSSH("Bad Type Error : {} instead of str or list".format(str(type(element))))
        else:
            if type(element) is str and keyWord is None:
                if server:
                    found = self.remoteCompFiles.patternSearch(element)
                else:
                    found = self.localCompFiles.patternSearch(element)
            elif type(element) is str and type(keyWord) is str:
                if server:
                    found = self.remoteCompFiles.patternSearch(element, keyWord)
                else:
                    found = self.localCompFiles.patternSearch(element, keyWord)
            else:
                raise exceptionSSH("Bad Type Error: {} or {} is not str".format(str(type(element))), str(type(keyWord)))

        return found

    def extensionFw(self):
        return fw.folderView.extension()

    def getConf(self): #Modified
        """Method which reads the config file and return a list of the settings for the SRP"""
        config = self.configuration()
        maConf = config.exportConfiguration()
        if not maConf[0]:
            raise exceptionSSH("Incomplete configuration")
        else:
            return maConf[1]

class filesConfiguration:
    """ Configuration of the SRP and the config files of the SRP:
        There is two important parts.
            1- Configure the SRP with a good and strong config file
            2- Verify the configuration files when needed"""
    def __init__(self, configuration = {}):
        # 1 - server Directory | 2 - Client Directory | 3 - Depth | 4 - Server Address
        self.configuration = configuration
        self.configFile = "srpconf.json"
        if(pth.isfile(self.configFile)):
            self.readConfiguration()

    def exportConfiguration(self):
        """Method giving back a configuration list readable by the whole program"""
          #On digère la configuration pour la rendre accessible aux autres classes
        if self.readConfiguration():
            return (True, self.configuration) #Tout marche bien, on retorune la configuration
        else:
            return (False, None) #Rien ne va, on ne retorune rien qu'une erreur à traiter

    def readConfiguration(self):
        """Method reading the configuration file line by line"""
        os.chdir(pwdBasic)
          #On récupère la configuration dans un premier temps dans le format 'raw'
        if self.isConfig():
            config = open(self.configFile, "r")
            data = config.read()
            self.configuration = js.loads(data)
            config.close()
            return True #On a réussit à lire la configuration, elle existe
        else:
            return False #On n'a pas réussi à lire la configuration

    def writeConfiguration(self):
        """Method writting the attributes of this class in the configuration file"""
        os.chdir(pwdBasic)
            #On écrit tout d'abord la configuration dans le fichier configuration
        try:
            config = open(self.configFile, "w")
            config.write(js.dumps(self.configuration))
            config.close()
            #Désormais, on applique la configuration en créant les dépendances en local:
        except Exception as e:
            raise exceptionSSH(e)
        self.clientConfigCreation()

    def isConfig(self):
        """Method verifying that the config file exists"""
        os.chdir(pwdBasic)
        return pth.isfile(self.configFile)

    def clientConfigCreation(self):
        """Method verifying that the client part of the SRP exists (attempts to create it if needed)"""
        if not pth.isdir(self.configuration["Local"]):
            try:
                os.mkdir(self.configuration["Local"])
                return True
            except Exception as e:
                raise exceptionSSH(e)
        return True

    def serverConfigCreation(self, conn):
        """Method verifying and creating the server-side root
            - conn : An instance of connectionProtocol"""
        conn.createFold(self.configuration["Remote"])

    def configFileServer(self, pathServer, folder = None):
        """Method which configure the path of the SRP file on the server
            - pathServer : the path of the SRP root on the server"""
            #Should retrieve the full path on the server
        if type(pathServer) is not str:
            raise TypeError("Should be a str object")

        elif pathServer.strip()[:1] == "/":
                if folder is None:
                    self.configuration["Remote"] = pth.join(pathServer.strip(), "SRP")
                else:
                    self.configuration["Remote"] = pth.join(pathServer.strip(), "SRP", folder)
        else:
            raise exceptionSSH(pathServer + "Not a valid filename")
        #self.configuration[-1].strip() #What's the use?

    def configFileClient(self, pathClient):
        """Method which configure the path of the SRP file on the client computer, and will
            traduct a relative path in a full path
            - pathClient : The path of the SRP root on the client computer"""
        if type(pathClient) is not str:
            raise TypeError("Should be a str object")

        elif pathClient.strip()[:2] == "~/":
            pth_tmp = pth.join(pth.join(pth.expanduser("~"), pathClient.strip()[2:]))

        elif pathClient.strip()[:1] == "/":
            pth_tmp = pathClient
        else:
            raise exceptionSSH(pathClient + "Not a valid filename")

        self.configuration["Local"] = pth_tmp

    def configAddressServer(self, address):
        """Method which configure the address of the server
            - address : The address of the server"""
        if type(address) is not str:
            raise TypeError("Should be str object")

        elif len(address) == 0:
            raise exceptionSSH(address + "Server address cannot be empty")

        else:
            self.configuration["Address"] = address.strip()

    def configRecursion(self, recurs):
        """ Method which configure the number of recursion the SRP does when exploring a filesystem. i.e. How deep the SRP goes in imbricated directories
            - recurs : The number of recursion """
        if type(recurs) is not str and type(recurs) is not int:
            raise TypeError(str(type(recurs)) + " should be an int or str object")

        elif int(recurs) < 0:
            raise exceptionSSH(str(recurs) + "Cannot be a negative number")

        else:
            self.configuration["Recursion"] = str(recurs)

    def deleteConfig(self):
        """Method deleting all the files related to the SRP (uninstallation)"""
        os.chdir(pwdBasic)
        if self.isConfig():
            myConf = self.exportConfiguration()
            with tarfile.open(myConf[1], "w:gz") as tar:
                 tar.add(myConf[1], arcname=pth.basename(myConf[1]))
            os.removedirs(myConf[1])
            os.remove(self.configFile)

    def helpConfig(self):
        """ Help utilitary on the main functionality of this class"""
        print("Welcome in the help pannel of the Server Research Program's configuration")
        print("            h -p    for the help on paths/files")
        print("            h -a    for the help on Ip and network configuration")
        print("            h -t    to get advices and why I'm asking all this stuff")
        print("            q       to quit this help menu")
        helpPanel = input(">>h>> ")
        while helpPanel != 'q':
            if helpPanel == "h -p":
                print("\n")
                print("-----------------------Welcome in the Tiny Help Pannel----------------------------")
                print("\033[36mRelative path\033[0m is: ~/ (It means /home/user at the end)")
                print("\033[36mAbsolute/Full path\033[0m is: /home/user (you're specifying the entire path from the root")
                print("   which is /)\n")
                print("    - The first line of the SRP config file is the server directory where")
                print("      is located the folder where we are keeping your files")
                print("    - The second line is where we store the path of the local files of the SRP\n")
                print("By default, the name of the config file is .config.cfg (Very original name)")
                print("\033[31mYou SHALL not rename it directly. Instead, you should change the path in ")
                print("the constructor (self.configFile)\033[0m\n ")
                print("By default, the server and the client paths to the folders needed are empty. This")
                print("means that you have to configurate the SRP with this utility\033[31m at least once.\033[0m")
                print(" This help is specific to the configuration. For bug reports, talk to the developer ")
                print("--------------------------------Good luck and enjoy!-------------------------------")
            elif helpPanel == "h -a":
                print("\n")
                print("-----------------------------Welcome in the Tiny Help Panel-------------------------------")
                print("The SRP can use raw IP (like 192.168.1.1) but it's \033[31m firmly not recommended\033[0m")
                print("You will get unexpected errors when your IP will change (at the reboot of your router for")
                print("example). Instead, you should use Dyn-DNS or No-IP server, which are pretty good, free")
                print("and easy to use. ")
                print("    - The address is located at the third line of the SRP config file")
                print("By default, it will be empty, as said in the help pannel for paths, you shall")
                print("\033[31mrun this utility at least once to configure properly the SRP\033[0m")
                print("---------------------------------Good Luck and enjoy!-------------------------------------")
            elif helpPanel == "h -t":
                print("------------------------------Welcome in the Tiny Help Panel-------------------------------")
                print("    - If you give to the SRP the relative path, \033[31mit will automatically create the full path\033[0m")
                print("      It's why we are strongly recommanding you to put the relative path.")
                print("---------------------------------Good Luck and enjoy!--------------------------------------")
            else:
                print("\033[31mUnknown command\033[0m")
            helpPanel = input(">>h>> ")
        print("You're sent back to the configuration utility. If you want to come back to this help menu, type h or H.")
        print("\033[33mGood Luck !\033[0m")
